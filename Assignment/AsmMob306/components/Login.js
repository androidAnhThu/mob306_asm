/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet, Text, View, ImageBackground, Image, 
  TextInput, Dimensions, TouchableOpacity
} from 'react-native';

import bgImage from '../images/background.jpg'
import logo from '../images/logo.png'

const {width: WIDTH} = Dimensions.get('window')

export default class Login extends Component {
  render() {
    return (
      <ImageBackground source={bgImage} style={styles.backgroundContainer}>
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.logo} />
          <Text style={styles.logoText}>REACT NATIVE</Text>
        </View>

        <View style={styles.inputContainer}>
          <TextInput
          style={styles.input}
          placeholder={'Username'}
          placeholderTextColor={'#663366'}
          underlineColorAndroid='transparent'/>
        </View>

        <View style={styles.inputContainer}>
          <TextInput
          style={styles.input}
          placeholder={'Password'}
          secureTextEntry={true}
          placeholderTextColor={'#663366'}
          underlineColorAndroid='transparent'/>
        </View>

        <TouchableOpacity style={styles.btnLogin}>
          <Text style={styles.text}>Login</Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  logoContainer: {
    alignItems: 'center',
    marginBottom:50
  },

  logo:{
    width: 200,
    height: 200
  },

  logoText:{
    color: 'purple',
    fontSize: 25,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.5
  },

  inputContainer:{
    marginTop: 10
  },

  input:{
    width: WIDTH -55 ,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor:'#CC99FF',
    color: '#FFFFFF',
    marginHorizontal: 25
  },

  btnLogin:{
    width: WIDTH -55 ,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#663366',
    justifyContent: 'center',
    marginTop: 20
  },
  text:{
    color: '#ffffff',
    fontSize: 20,
    textAlign: 'center'
  }
});
